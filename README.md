# Descrição
O objetivo é apresentar os conhecimentos na construção de um front end utilizando as seguintes pilhas tecnológicas:

* AngularJS
* Material Design
* Este projeto foi gerado com [CLI Angular] versão 1.6.5

## Crud API DropWizard
https://github.com/wstlima/dropwizard-mysql-rest

## Servidor de desenvolvimento
Rode o comando ng serve para um servidor dev. Navegue até http: // localhost: 4200 / O aplicativo será recarregado automaticamente se você alterar qualquer um dos arquivos de origem.

## Geração Automática
Rode `ng generate component component-name` para gerar um novo componente. Você também pode usar `ng generate directive|pipe|service|class|guard|interface|enum|module`

## Desenvolvimento
Rode `npm i` para baixar os pacotes. e depois
Rode `ng serve`

## github-pages
Isto é publicado para o ramo de páginas gh através de um plugin externo.
angular-cli-ghpages
https://github.com/angular-schule/angular-cli-ghpages
Comandos:
ng build --prod --base-href="/crud-angular5/"
ng build --base-href="/crud-angular5/"
angular-cli-ghpages

## Execução de testes unitários
Rode `ng test` para executar os testes unitários via [Karma](https://karma-runner.github.io).

## Rodando testes para end-to-end
Rode `ng e2e` para executar o end-to-end testes via [Protractor](http://www.protractortest.org/).
