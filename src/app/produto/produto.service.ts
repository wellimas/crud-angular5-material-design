import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { of } from "rxjs/observable/of";
import { catchError, map } from "rxjs/operators";
import { Produto } from "./produto";

@Injectable()
export class ProdutoService {
    private baseUrl = "http://localhost:4000/produto";
    constructor(private http: HttpClient) {}

    getProdutos(): Observable<Produto[]> {
        return this.http
          .get<Produto[]>("http://localhost:4000/produto")
          .pipe(
            map(data => data),
            catchError(this.handleError("getProdutos", []))
          );
      }


      addProduto(produto: Produto): Observable<Produto> {
        return this.http
          .post("http://localhost:4000/produto", produto)
          .pipe(catchError(this.handleError<Produto>("addProduto")));
      }    

      deleteProduto(id: string): Observable<any> {
        const url = `${this.baseUrl}/${id}`;
        return this.http
          .delete(url)
          .pipe(catchError(this.handleError<any>("deleteProduto")));
      }  

      getProdutoById(id: string): Observable<Produto> {
        const url = `${this.baseUrl}/${id}`;
        return this.http
          .get<Produto>(url)
          .pipe(catchError(this.handleError<Produto>(`getProdutoById = ${id}`)));
      }
    
      updateProduto(produto: Produto): Observable<any> {
        const url = `${this.baseUrl}/${produto["id"]}`;
        return this.http
          .put(url, produto)
          .pipe(catchError(this.handleError<any>("updateProduto")));
      }      


    private handleError<T>(operation = "operation", result?: T) {
        return (error: any): Observable<T> => {
            console.error(error);
            return of(result as T);
        };
    }  
    
    


}
