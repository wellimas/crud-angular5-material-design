import { Component, OnInit, Input } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { ProdutoService } from "../produto.service";
import { Produto } from "../produto";
import { GlobalsService } from "../../globals.service";
import { Location } from "@angular/common";

@Component({
    selector: "produto-detail",
    templateUrl: "./produto-detail.component.html",
    styleUrls: ["./produto-detail.component.scss"]
  })
  export class ProdutoDetailComponent implements OnInit {
    produto: Produto;
    constructor(
      private location: Location,
      private route: ActivatedRoute,
      private produtoSvc: ProdutoService,
      private globals: GlobalsService
    ) {}
  
    ngOnInit() {
      this.getProdutoDetails();
    }
  
    getProdutoDetails(): void {
      this.globals.loading = true;
      const id = this.route.snapshot.paramMap.get("id");
      this.produtoSvc.getProdutoById(id).subscribe(data => {
        this.produto = data;
        this.globals.loading = false;
      });
    }
  
    updateProduto(): void {
      this.globals.loading = true;
      this.produtoSvc.updateProduto(this.produto).subscribe(() => {
        this.globals.loading = false;
        this.goBack();
      });
    }
  
    goBack(): void {
        this.location.back();
    }


  }



