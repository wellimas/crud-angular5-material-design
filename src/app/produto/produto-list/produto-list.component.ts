// import { Component, OnInit, AfterViewInit, ChangeDetectorRef } from "@angular/core";
import { Component, OnInit } from "@angular/core";

import { ProdutoService } from "../produto.service";
import { Produto } from "../produto";
import { GlobalsService } from "../../globals.service";

@Component({
    selector: "produto-list",
    templateUrl: "./produto-list.component.html",
    styleUrls: ["./produto-list.component.scss"]
  })
  export class ProdutoListComponent implements OnInit {

    hoverOpacity: number = 0.5;
    produtos: Produto[];
    selectedProduto: Produto;

    constructor(
        // private cdr: ChangeDetectorRef,
        private produtoService: ProdutoService,
        private globals: GlobalsService
      ) {}

    ngOnInit() {
      this.getProdutos();
    }

    getProdutos(): void {
        this.globals.loading = true;
        setTimeout(() => {
          this.produtoService.getProdutos().subscribe(data => {
            this.produtos = data;
            this.globals.loading = false;
            // this.cdr.detectChanges();
          });
        });
      }


      deleteProduto(produto: Produto): void {
        console.log("id: ",produto["id"]);
        this.globals.loading = true;
        this.produtoService.deleteProduto(produto["id"]).subscribe(() => {
          this.getProdutos();
          // this.globals.loading = false;
        });
      }      
  }