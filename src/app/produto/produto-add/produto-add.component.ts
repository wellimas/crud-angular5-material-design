import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";

import { ProdutoService } from "../produto.service";
import { Produto } from "../produto";
import { GlobalsService } from "../../globals.service";



  @Component({
    selector: "produto-add",
    templateUrl: "./produto-add.component.html",
    styleUrls: ["./produto-add.component.scss"]
  })


  export class ProdutoAddComponent implements OnInit {
    produto: Produto;
    constructor(
      private location: Location,
      private produtoSvc: ProdutoService,
      private globals: GlobalsService
    ) {}
  
    ngOnInit() {
      this.produto = new Produto("");
    }

    addProduto(): void {
        this.globals.loading = true;
        this.produtoSvc.addProduto(this.produto).subscribe(() => {
          this.globals.loading = false;
          this.goBack();
        });
    }

  
    goBack(): void {
      this.location.back();
    }    

}
  