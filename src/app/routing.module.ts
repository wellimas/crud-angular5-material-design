import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { ProdutoListComponent } from "./produto/produto-list/produto-list.component";
import { ProdutoDetailComponent } from "./produto/produto-detail/produto-detail.component";
import { ProdutoAddComponent } from "./produto/produto-add/produto-add.component";

const routes: Routes = [
  {
    path: "produtos",
    component: ProdutoListComponent
  },
  {
    path: "produtos/add",
    component: ProdutoAddComponent
  },
  {
    path: "produtos/detail/:id",
    component: ProdutoDetailComponent
  },
  {
    path: "",
    redirectTo: "/produtos",
    pathMatch: "full"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class RoutingModule {}
