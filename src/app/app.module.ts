import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from '@angular/forms';

import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { CustomMaterialModule } from "./custom-material/custom-material.module";
import { AppComponent } from "./app.component";
import { RoutingModule } from './/routing.module';
import { GlobalsService } from './globals.service';
import { HoverEffectDirective } from './hover-effect.directive';

import { ProdutoListComponent } from "./produto/produto-list/produto-list.component";
import { ProdutoAddComponent } from "./produto/produto-add/produto-add.component";
import { ProdutoService } from "./produto/produto.service";
import { ProdutoDetailComponent } from "./produto/produto-detail/produto-detail.component";


@NgModule({
  declarations: [AppComponent, ProdutoDetailComponent, ProdutoAddComponent, ProdutoListComponent, HoverEffectDirective],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    CustomMaterialModule,
    RoutingModule
  ],
  providers: [ProdutoService, GlobalsService],
  bootstrap: [AppComponent]
})
export class AppModule {}
